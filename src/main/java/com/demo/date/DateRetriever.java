package com.demo.date;

import java.util.Date;

import org.springframework.stereotype.Component;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Component
public class DateRetriever {

	

	@HystrixCommand(fallbackMethod="retrieveFallbackDateinfo")
	public String dateInfo() {
		return new Date().toString();
	}

	
	
	public String retrieveFallbackDateinfo() {
		return "date 18-01-2014";
	}
	
	
}
